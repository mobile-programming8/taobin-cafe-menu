import 'dart:io';

bool isNotFinish = true;
List<Drinks> drinks = [];
void main() {
  while (isNotFinish) {
    showMenu();
    recieveMenu();
  }
  printLine();
  int totalPrice = 0;
  for (Drinks drink in drinks) {
    drink.showInfo();
    totalPrice += drink.getPrice();
  }
  print("$totalPrice฿ : Total Price");
}

void recieveMenu() {
  String? menu = stdin.readLineSync();
  switch (menu) {
    case "1":
      hotCoffeeMenu();
      break;
    case "2":
      icedCoffeeMenu();
      break;
    case "3":
      hotMilkMenu();
      break;
    case "4":
      icedMilkMenu();
      break;
    case "5":
      hotTeaMenu();
      break;
    case "6":
      icedTeaMenu();
      break;
    case "7":
      proteinShakeMenu();
      break;
    case "8":
      fruityDrinkMenu();
      break;
    case "9":
      sodaMenu();
      break;
    default:
    isNotFinish = false;
  }
}

void sodaMenu() {
  printLine();
  print("1. Pepsi");
  print("2. Iced Limeade Soda");
  print("3. Iced Lychee Soda");
  print("4. Iced Strawberry Soda");
  print("5. Iced Cannabis Soda");
  print("6. Iced Plum Soda");
  print("7. Iced Ginger Soda");
  print("8. Iced Blueberry Soda");
  print("9. Iced Sala Soda");
  print("10.Iced Lime Sala Soda");
  String? select = stdin.readLineSync();
  switch (select) {
    case "1":
      Drinks drink = Drinks("Pepsi", 40, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "2":
      Drinks drink = Drinks("Iced Limeade Soda", 40, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "3":
      Drinks drink = Drinks("Iced Lychee Soda", 40, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "4":
      Drinks drink = Drinks("Iced Strawberry Soda", 40, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "5":
      Drinks drink = Drinks("Iced Cannabis Soda", 40, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "6":
      Drinks drink = Drinks("Iced Plum Soda", 40, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "7":
      Drinks drink = Drinks("Iced Ginger Soda", 40, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "8":
      Drinks drink = Drinks("Iced Blueberry Soda", 40, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "9":
      Drinks drink = Drinks("Iced Sala Soda", 40, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "10":
      Drinks drink = Drinks("Iced Lime Sala Soda", 40, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    
    default:
  }
}

void fruityDrinkMenu() {
  printLine();
  print("1.Hot Limade");
  print("2.Iced Limeade");
  print("3.Iced Lychee");
  print("4.Iced Strawberry");
  print("5.Iced Blueberry");
  print("6.Iced Plum");
  print("7.Iced Mango");
  print("8.Iced Sala");
  print("9.Iced Limeade sala");
  String? select = stdin.readLineSync();
  switch (select) {
    case "1":
      Drinks drink = Drinks("Hot Limade", 45, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "2":
      Drinks drink = Drinks("Iced Limeade", 45, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "3":
      Drinks drink = Drinks("Iced Lychee", 45, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "4":
      Drinks drink = Drinks("Iced Strawberry", 45, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "5":
      Drinks drink = Drinks("Iced Blueberry", 45, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "6":
      Drinks drink = Drinks("Iced Plum", 45, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "7":
      Drinks drink = Drinks("Iced Mango", 45, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "8":
      Drinks drink = Drinks("Iced Sala", 45, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "9":
      Drinks drink = Drinks("Iced Limeade sala", 45, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    
    default:
  }
}

void proteinShakeMenu() {
  printLine();
  print("1. Matcha Protein Shake");
  print("2. Chocolate Protein Shake");
  print("3. Strawberry Protein Shake");
  print("4. Espresso Protein Shake");
  print("5. Thai Tea Protein Shake");
  print("6. Brown Sugar Protein Shake");
  print("7. Taiwanese Tea Protein Shake");
  print("8. Caramel Protei Shake");
  print("9. Plain Protein Shake");
  print("10.Milk Shake");
  String? select = stdin.readLineSync();
  switch (select) {
    case "1":
      Drinks drink = Drinks("Matcha Protein Shake", 45, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "2":
      Drinks drink = Drinks("Chocolate Protein Shake", 45, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "3":
      Drinks drink = Drinks("Strawberry Protein Shake", 45, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "4":
      Drinks drink = Drinks("Espresso Protein Shake", 45, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "5":
      Drinks drink = Drinks("Thai Tea Protein Shake", 45, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "6":
      Drinks drink = Drinks("Brown Sugar Protein Shake", 45, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "7":
      Drinks drink = Drinks("Taiwanese Tea Protein Shake", 45, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "8":
      Drinks drink = Drinks("Caramel Protei Shake", 45, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "9":
      Drinks drink = Drinks("Plain Protein Shake", 45, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "10":
      Drinks drink = Drinks("Milk Shake", 45, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    
    default:
  }
}

void icedTeaMenu() {
  printLine();
  print("1. Iced Chrysanthemum Tea");
  print("2. Iced Thai Milk Tea");
  print("3. Iced Taiwanese Milk Tea");
  print("4. Iced Matcha Latte");
  print("5. Iced Tea");
  print("6. Iced Kokuto Tea");
  print("7. Iced Limeade Tea");
  print("8. Iced Lychee Tea");
  print("9. Iced Strawberry Tea");
  print("10.Iced Blueberry Tea");
  String? select = stdin.readLineSync();
  switch (select) {
    case "1":
      Drinks drink = Drinks("Iced Chrysanthemum Tea", 40, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "2":
      Drinks drink = Drinks("Iced Thai Milk Tea", 40, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "3":
      Drinks drink = Drinks("Iced Taiwanese Milk Tea", 40, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "4":
      Drinks drink = Drinks("Iced Matcha Latte", 40, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "5":
      Drinks drink = Drinks("Iced Tea", 40, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "6":
      Drinks drink = Drinks("Iced Kokuto Tea", 40, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "7":
      Drinks drink = Drinks("Iced Limeade Tea", 40, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "8":
      Drinks drink = Drinks("Iced Lychee Tea", 40, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "9":
      Drinks drink = Drinks("Iced Strawberry Tea", 40, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "10":
      Drinks drink = Drinks("Iced Blueberry Tea", 40, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    
    default:
  }
}

void hotTeaMenu() {
  printLine();
  print("1. Hot Chrysanthemum Tea");
  print("2. Hot Thai Milk Tea");
  print("3. Hot Taiwanese Tea");
  print("4. Hot Matcha Latte");
  print("5. Hot Black Tea");
  print("6. Hot Kokuto Tea");
  print("7. Hot Lime Tea");
  print("8. Hot Lychee Tea");
  print("9. Hot Strawberry Tea");
  print("10.Hot Blueberry Tea");
  String? select = stdin.readLineSync();
  switch (select) {
    case "1":
      Drinks drink = Drinks("Hot Chrysanthemum Tea", 40, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "2":
      Drinks drink = Drinks("Hot Thai Milk Tea", 40, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "3":
      Drinks drink = Drinks("Hot Taiwanese Tea", 40, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "4":
      Drinks drink = Drinks("Hot Matcha Latte", 40, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "5":
      Drinks drink = Drinks("Hot Black Tea", 40, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "6":
      Drinks drink = Drinks("Hot Kokuto Tea", 40, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "7":
      Drinks drink = Drinks("Hot Lime Tea", 40, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "8":
      Drinks drink = Drinks("Hot Lychee Tea", 40, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "9":
      Drinks drink = Drinks("Hot Strawberry Tea", 40, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "10":
      Drinks drink = Drinks("Hot Blueberry Tea", 40, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    
    default:
  }
}

void icedMilkMenu() {
  printLine();
  print("1.Iced Caramel Milk");
  print("2.Iced Kokuto Milk");
  print("3.Iced Cocoa");
  print("4.Iced Caramel Cocoa");
  print("5.Iced Pink Milk");
  String? select = stdin.readLineSync();
  switch (select) {
    case "1":
      Drinks drink = Drinks("Iced Caramel", 45, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "2":
      Drinks drink = Drinks("Iced Kokuto Milk", 45, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "3":
      Drinks drink = Drinks("Iced Cocoa", 45, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "4":
      Drinks drink = Drinks("Iced Caramel Cocoa", 45, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "5":
      Drinks drink = Drinks("Iced Pink Mlik", 45, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    default:
  }
}

void hotMilkMenu() {
  printLine();
  print("1.Hot Caramel Milk");
  print("2.Hot Kokuto Milk");
  print("3.Hot Cocoa");
  print("4.Hot Caramel Cocoa");
  print("5.Hot Milk");
  String? select = stdin.readLineSync();
  switch (select) {
    case "1":
      Drinks drink = Drinks("Hot Caramel", 40, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "2":
      Drinks drink = Drinks("Hot Kokuto Milk", 40, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "3":
      Drinks drink = Drinks("Hot Cocoa", 40, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "4":
      Drinks drink = Drinks("Hot Caramel Cocoa", 40, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "5":
      Drinks drink = Drinks("Hot Mlik", 40, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    default:
  }
}

void icedCoffeeMenu() {
  printLine();
  print("1. Dirty");
  print("2. Iced Espresso");
  print("3. Iced Americano");
  print("4. Iced Cafe' Latte");
  print("5. Iced Cappuccino");
  print("6. Iced Mocha");
  print("7. Iced Caramel Cafe' Latte");
  print("8. Iced Matcha Cafe' Latte");
  print("9. Iced Taiwanese Tea Cafe' Latte");
  print("10.Iced Hokuto Cafe' Latte");
  print("11.Iced Thai Tea Cafe' Latte");
  print("12.Iced Lychee Americano");
  print("13.Iced Cannabis Americano");
  String? select = stdin.readLineSync();
  switch (select) {
    case "1":
      Drinks drink = Drinks("Dirty", 45, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "2":
      Drinks drink = Drinks("Iced Espresso", 45, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "3":
      Drinks drink = Drinks("Iced Americano", 45, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "4":
      Drinks drink = Drinks("Iced Cafe' Latte", 45, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "5":
      Drinks drink = Drinks("Iced Cappuccino", 45, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "6":
      Drinks drink = Drinks("Iced Mocha", 45, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "7":
      Drinks drink = Drinks("Iced Caramel Cafe' Latte", 50, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "8":
      Drinks drink = Drinks("Iced Matcha Cafe' Latte", 50, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "9":
      Drinks drink = Drinks("Iced Taiwanese Tea Cafe' Latte", 50, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "10":
      Drinks drink = Drinks("Iced Hokuto Cafe' Latte", 50, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "11":
      Drinks drink = Drinks("Iced Thai Tea Cafe' Latte", 50, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "12":
      Drinks drink = Drinks("Iced Lychee Americano", 50, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "13":
      Drinks drink = Drinks("Iced Cannabis Americano", 50, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    default:
  }
}

void hotCoffeeMenu() {
  printLine();
  print("1. Espresso");
  print("2. Double Espresso");
  print("3. Hot Americano");
  print("4. Hot Cafe' Latte");
  print("5. Hot Cappucino");
  print("6. Hot Mocha");
  print("7. Hot Caramel Latte");
  print("8. Hot Taiwanese Tea Cafe' Latte");
  print("9. Hot Matcha Latte");
  print("10.Hot Kokuto Cafe' Latte");
  print("11.Hot Thai Tea Cafe' Latte");
  print("12.Hot Lychee Americano");
  String? select = stdin.readLineSync();
  switch (select) {
    case "1":
      Drinks drink = Drinks("Espresso", 40, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "2":
      Drinks drink = Drinks("Double Espresso", 50, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "3":
      Drinks drink = Drinks("Hot Americano", 40, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "4":
      Drinks drink = Drinks("Hot Cafe' Latte", 40, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "5":
      Drinks drink = Drinks("Hot Cappucino", 40, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "6":
      Drinks drink = Drinks("Hot Mocha", 40, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "7":
      Drinks drink = Drinks("Hot Caramel Latte", 50, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "8":
      Drinks drink = Drinks("Hot Taiwanese Tea Cafe' Latte", 45, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "9":
      Drinks drink = Drinks("Hot Matcha Latte", 45, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "10":
      Drinks drink = Drinks("Hot Kokuto Cafe' Latte", 50, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "11":
      Drinks drink = Drinks("Hot Thai Tea Cafe' Latte", 45, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    case "12":
      Drinks drink = Drinks("Hot Lychee Americano", 50, false, 0, 2, false, false, false, false);
      recieveCustomDrink(drink);
      return;
    default:
  }
}

void recieveCustomDrink(Drinks drink) {
  while(true) {
    showDrink(drink);
    String? custom = stdin.readLineSync();
    if(custom == "12"){
      return;
    }else if(custom == "13"){
      bool isAdded = false;
      for (Drinks tmpdrink in drinks) {
        if(isDrinkDuplicate(tmpdrink, drink)) {
          tmpdrink.amount++;
          isAdded = true;
          break;
        }
      }
      if(!isAdded) {
        drinks.add(drink);
      }
      return;
    }else if(custom == "14"){
      bool isAdded = false;
      for (Drinks tmpdrink in drinks) {
        if(isDrinkDuplicate(tmpdrink, drink)) {
          tmpdrink.amount++;
          isAdded = true;
          break;
        }
      }
      if(!isAdded) {
        drinks.add(drink);
      }
      isNotFinish = false;
      return;
    }
    customDrink(custom!, drink);
  }
}

bool isDrinkDuplicate(Drinks tmpdrink, Drinks drink) {
  if(tmpdrink.name == drink.name &&
  tmpdrink.price == drink.price &&
  tmpdrink.isSmoothie == drink.isSmoothie &&
  tmpdrink.sweetnessType == drink.sweetnessType &&
  tmpdrink.sweetnesslevel == drink.sweetnesslevel &&
  tmpdrink.isExtraShot == drink.isExtraShot &&
  tmpdrink.isBoba == drink.isBoba &&
  tmpdrink.wantStraw == drink.wantStraw &&
  tmpdrink.wantLid == drink.wantLid){
    return true;
  }
  return false;
}

void customDrink(String custom, Drinks drink) {
  switch (custom) {
    case "1":
      drink.isSmoothie = !drink.isSmoothie;
      break;
    case "2":
      if(drink.sweetnessType == 0){
        drink.sweetnessType = 1;
      }else{
        drink.sweetnessType = 0;
      }
      break;
    case "3":
      if(drink.sweetnessType == 2){
        drink.sweetnessType = 1;
      }else{
        drink.sweetnessType = 2;
      }
      break;
    case "4":
      if(drink.sweetnesslevel == 0){
        drink.sweetnesslevel = 1;
      }else{
        drink.sweetnesslevel = 0;
      }
      break;
    case "5":
      if(drink.sweetnesslevel == 0 || drink.sweetnesslevel == 1){
        drink.sweetnesslevel = 2;
      }else{
        drink.sweetnesslevel = 1;
      }
      break;
    case "6":
      if(drink.sweetnesslevel == 3 || drink.sweetnesslevel == 4){
        drink.sweetnesslevel = 2;
      }else{
        drink.sweetnesslevel = 3;
      }
      break;
    case "7":
      if(drink.sweetnesslevel == 4){
        drink.sweetnesslevel = 3;
      }else{
        drink.sweetnesslevel = 4;
      }
      break;
    case "8":
      drink.isExtraShot = !drink.isExtraShot;
      break;
    case "9":
      drink.isBoba = !drink.isBoba;
      break;
    case "10":
      drink.wantStraw = !drink.wantStraw;
      break;
    case "11":
      drink.wantLid = !drink.wantLid;
      break;
    default:
  }
}

void showDrink(Drinks drink) {
  printLine();
  print(drink.getName());
  printLine();
  print("Blended");
  if(drink.isSmoothie) {
    print("Smoothie (+฿5) </> 1.");
  }else{
    print("Smoothie (+฿5) < > 1.");
  }
  printLine();
  print("Sweetness Type");
  switch (drink.getSweetnessType()) {
    case 0:
      print("Sugar (Free)       </>");
      print("Honey (+฿5)        < > 2.");
      print("0 cal sugar (+฿10) < > 3.");
      break;
    case 1:
      print("Sugar (Free)       < > 2.");
      print("Honey (+฿5)        </>");
      print("0 cal sugar (+฿10) < > 3.");
      break;
    case 2:
      print("Sugar (Free)       < > 2.");
      print("Honey (+฿5)        < > 3.");
      print("0 cal sugar (+฿10) </>");
      break;
    default:
  }
  printLine();
  print("Sweetness Level");
  switch (drink.getSweetnesslevel()) {
    case 0:
      print("No Sugar   </>");
      print("Less Sweet < > 4.");
      print("Just Right < > 5.");
      print("Sweet      < > 6.");
      print("Very Sweet < > 7.");
      break;
    case 1:
      print("No Sugar   < > 4.");
      print("Less Sweet </>");
      print("Just Right < > 5.");
      print("Sweet      < > 6.");
      print("Very Sweet < > 7.");
      break;
    case 2:
      print("No Sugar   < > 4.");
      print("Less Sweet < > 5.");
      print("Just Right </>");
      print("Sweet      < > 6.");
      print("Very Sweet < > 7.");
      break;
    case 3:
      print("No Sugar   < > 4.");
      print("Less Sweet < > 5.");
      print("Just Right < > 6.");
      print("Sweet      </>");
      print("Very Sweet < > 7.");
      break;
    case 4:
      print("No Sugar   < > 4.");
      print("Less Sweet < > 5.");
      print("Just Right < > 6.");
      print("Sweet      < > 7.");
      print("Very Sweet </>");
      break;
    default:
  }
  printLine();
  print("Extra topping");
  if(drink.isExtraShot) {
    print("1 Shot of Espresso (+฿15) </> 8.");
  }else{
    print("1 Shot of Espresso (+฿15) < > 8.");
  }
  if(drink.isBoba) {
    print("Brown sugar konjac boba (+฿10) </> 9.");
  }else{
    print("Brown sugar konjac boba (+฿10) < > 9.");
  }
  printLine();
  print("Straw and Lid");
  if(drink.wantStraw){
    print("I want a straw </> 10.");
  }else{
    print("I want a straw < > 10.");
  }
  if(drink.wantLid) {
    print("I want a lid </> 11.");
  }else{
    print("I want a lid < > 11.");
  }
  printLine();
  print("Cancel 12.");
  print("Put to cart and continue 13.");
  print("Buy now (${drink.getPrice()}฿) 14.");
}

void printLine(){
  print("-----------------");
}

void showMenu() {
  printLine();
  print("1.Hot Coffee");
  print("2.Iced Coffee");
  print("3.Hot Milk");
  print("4.Iced Milk");
  print("5.Hot Tea");
  print("6.Iced Tea");
  print("7.Protein Shakes");
  print("8.Fruity Drinks");
  print("9.Soda");
  print("0.Finish");
}

class Drinks {
  String name;
  int price;
  bool isSmoothie;
  int sweetnessType;
  int sweetnesslevel;
  bool isExtraShot;
  bool isBoba;
  bool wantStraw;
  bool wantLid;
  int amount = 1;
  Drinks(this.name, this.price, this.isSmoothie, this.sweetnessType,
  this.sweetnesslevel, this.isExtraShot, this.isBoba, this.wantStraw, this.wantLid);

 String getName() {
  return name;
 }
 
 int getPrice() {
  int totalPrice = price;
  if(isSmoothie){
    totalPrice += 5;
  }
  if(sweetnessType == 1){
    totalPrice += 5;
  }else if(sweetnessType == 2){
    totalPrice += 10;
  }
  if(isExtraShot){
    totalPrice += 15;
  }
  if(isBoba){
    totalPrice += 10;
  }
  return totalPrice * amount;
 }

 bool getIsSmoothie() {
  return isSmoothie;
 }

 int getSweetnesslevel() {
  return sweetnesslevel;
 }

 int getSweetnessType() {
  return sweetnessType;
 }

 bool getIsExtraShot() {
  return isExtraShot;
 }

 bool getIsBoba() {
  return isBoba;
 }

 bool getWantStraw() {
  return wantStraw;
 }

 bool getWantLid() {
  return wantLid;
 }

 void setName(String name) {
  this.name = name;
 }
 
 void setPrice(int price) {
  this.price = price;
 }

 void setIsSmoothie(bool isSmoothie) {
  this.isSmoothie = isSmoothie;
 }

 void setSweetnesslevel(int sweetnesslevel) {
  this.sweetnesslevel = sweetnesslevel;
 }

 void setSweetnessType(int sweetnessType) {
  this.sweetnessType = sweetnessType;
 }

 void setIsExtraShot(bool isExtraShot) {
  this.isExtraShot = isExtraShot;
 }

 void setIsBoba(bool isBoba) {
  this.isBoba = isBoba;
 }

 void setWantStraw(bool wantStraw) {
  this.wantStraw = wantStraw;
 }

 void setWantLid(bool wantLid) {
  this.wantLid = wantLid;
 }
 
  void showInfo() {
    stdout.write("${getPrice()}฿ : ");
    stdout.write(amount);
    stdout.write('x ');
    stdout.write(name);
    if(isBoba || isExtraShot || isSmoothie || sweetnessType > 0) {
      stdout.write(' (');
      if(isSmoothie) {
        stdout.write('Smoothie');
      }
      if(sweetnessType > 0) {
        if(isSmoothie) {
          stdout.write(', ');
          switch (sweetnessType) {
            case 1:
              stdout.write('Honey');
              break;
            case 2:
              stdout.write('0 cal sugar');
              break;
            default:
          }
        }else{
          switch (sweetnessType) {
            case 1:
              stdout.write('Honey');
              break;
            case 2:
              stdout.write('0 cal sugar');
              break;
            default:
          }
        }
      }
      if(isExtraShot) {
        if(isSmoothie || sweetnessType > 0) {
          stdout.write(', Extra shot');
        }else{
          stdout.write('Extra shot');
        }
      }
      if(isBoba) {
        if(isSmoothie || isExtraShot || sweetnessType > 0) {
          stdout.write(', Brown sugar boba');
        }else{
          stdout.write('Brown sugar boba');
        }
      }
      stdout.write(')');
    }
    if(wantStraw) {
      if(wantLid) {
        stdout.write(' With straw and lid');
      }else{
        stdout.write(' with straw');
      }
    }else{
      if(wantLid) {
        stdout.write(' With lid');
      }
    }
    if(sweetnesslevel != 2) {
      stdout.write(' Sweetness: ');
      switch (sweetnesslevel) {
        case 0:
          stdout.write('No Sugar');
          break;
        case 1:
          stdout.write('Less Sweet');
          break;
        case 3:
          stdout.write('Sweet');
          break;
        case 4:
          stdout.write('Very Sweet');
          break;
        default:
      }
    }
    print("");
  }
}